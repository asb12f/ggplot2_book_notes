# Overview

This repository contains RMD documents for my work and notes while going through Hadley Wickam's 2005 book 'ggplot2'. This includes adjustments to his original code to reflect changes in the ggplot2 package since it was origially published. 