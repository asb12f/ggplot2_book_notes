
email = ""
password = ""

cookie <- login(email=email, password=password); cookie
data <- get_activity_data(cookie, end_date="2016-04-12"); summary(data)


data <- get_daily_data(cookie, what="steps", start_date="2016-04-01", end_date="2016-04-13")
data <- get_sleep_data(cookie, start_date="2016-03-13", end_date="2016-04-13")

dailySteps <- get_intraday_data(cookie, what="steps", date="2016-04-13")

today <- Sys.Date()
yesterday <- Sys.Date()-1
TWoDays <- Sys.Date()-2
ThreeDays <- Sys.Date()-3
FourDays <- Sys.Date()-4
FiveDays <- Sys.Date()-5
SixDays <- Sys.Date()-6
SevenDays <- Sys.Date()-7

format(today, format="%Y-%m-%d")
format(yesterday, format="%Y-%m-%d")
format(TWoDays, format="%Y-%m-%d")
format(ThreeDays, format="%Y-%m-%d")
format(FourDays, format="%Y-%m-%d")
format(FiveDays, format="%Y-%m-%d")
format(SixDays, format="%Y-%m-%d")
format(SevenDays, format="%Y-%m-%d")

today <- as.character(today)
yesterday <- as.character(yesterday)
TWoDays <- as.character(TWoDays)
ThreeDays <- as.character(ThreeDays)
FourDays <- as.character(FourDays)
FiveDays <- as.character(FiveDays)
SixDays <- as.character(SixDays)
SevenDays <- as.character(SevenDays)

TodayHR <- get_intraday_data(cookie, what="heart-rate", date=today)
YesterdayHR <- get_intraday_data(cookie, what="heart-rate", date=yesterday)
TwoDaysHR <- get_intraday_data(cookie, what="heart-rate", date=TWoDays)
ThreeDaysHR <- get_intraday_data(cookie, what="heart-rate", date=ThreeDays)
FourDaysHR <- get_intraday_data(cookie, what="heart-rate", date=FourDays)
FiveDaysHR <- get_intraday_data(cookie, what="heart-rate", date=FiveDays)
SixDaysHR <- get_intraday_data(cookie, what="heart-rate", date=SixDays)
SevenDaysHR <- get_intraday_data(cookie, what="heart-rate", date=SevenDays)

plot(TodayHR$bpm ~ dailyHR$time, type='l', main="Today's HR Data", 
     ylab="BPM", xlab="Time", col='red')
lines(YesterdayHR$bpm ~ dailyHR$time, col='green', type='l')
lines(TwoDaysHR$bpm ~ dailyHR$time, col='green', type='l')
lines(ThreeDaysHR$bpm ~ dailyHR$time, col='green', type='l')
lines(FourDaysHR$bpm ~ dailyHR$time, col='green', type='l')
lines(FiveDaysHR$bpm ~ dailyHR$time, col='green', type='l')

HRData <- cbind(TodayHR$bpm, YesterdayHR$bpm, ThreeDaysHR$bpm, 
                FourDaysHR$bpm, FiveDaysHR$bpm, SixDaysHR$bpm,
                SevenDaysHR$bpm)

names(HRData)[1] <- 'Today'
names(HRData)[2] <- "Yesterday"
names(HRData)[3] <- "3 Days"
summary(HRData)

cor(HRData)
pairs(HRData, col='blue')
