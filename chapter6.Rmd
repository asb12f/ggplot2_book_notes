---
title: "Chapter 6"
author: "Andrew"
output: html_document
---

```{r, echo=FALSE}
library(ggplot2)
library(plyr)
```

# Chapter 6 - Scales, Axes, and Legends
## Intro
Scales map data to aesthetics.
Scales are important as they produce a guide that allows inverse mapping from aesthetic space to data space, so viewers can read data values off the plot.

Scaling takes place in three steps - transformation, training, and mapping.
Trasformations area pplied to continuous data domains and transform teh data into useful forms, like logs or square roots.
Training is the stage where the scale is learned;  thsi can be straightforward like determining the minimum and maximum values of a continuous variable or the levels of a categorical variable, or complex like and reflect multipe layers across multiple data sets.
Training can be specified manually using the limits arguments, which will assign any values outside the training limits as NA.
Finlaly, mapping is the process of applying the scaling function to teh actual aesthetic values.

Every aesthetic has a default scale, and default scales are added when a plot is initialized or new layers are added.
This can create problems if there is a mismatch between the variable and scale type if you later change the data; in these instances you must manually correct the scale. For example:

```{r}
plot <- qplot(cty,hwy,data=mpg)
plot

# This doesn't work right because there is a mismatch between the
# variable type and the scale
plot+aes(x=as.integer(drv))

# Manual correction
plot + aes(x=drv) + scale_x_discrete()

```

If we need to build a diffeent scale, we need to manually add it to the plot. 
For example:

```{r}
p <- qplot(sleep_total, sleep_cycle, data=msleep, colour=vore)
p

# explicitly add the default scale
p + scale_colour_hue()

# adjust the parameters of the default, chaning the appearance of the legend
p + scale_colour_hue("What does\nit eat?", 
                     breaks=c("herbi","carni","omni",NA),
                     labels=c("plants","meat","both", "Don't know"))

# Use a different scale
p + scale_colour_brewer(palette = "Set1")
```

Scales can be divided into four groups - position scales, color scales, manual scales, and aesthetic scales.

## Common Arguments
### Name
To add labels for axis or legends, simply supply text strings.
There are some helper functions, including xlab(), ylab(), and labs().

```{r}
p <- qplot(cty, hwy, data=mpg, colour=displ)
p
p + scale_x_continuous("City MPG")
p + xlab("City MPG")
p + ylab("Highway MPG")
p + labs(x="City MPG", y="Highway MPG", colour="Displacement")
p + xlab(expression(frac(miles,gallon)))
```

### Limits
Limites fix teh domain fot he scale. 
Continuous scales require a vector oflength two, discrete scales take a character vector.

### Breaks and Labels

Breaks control which values appear on the axis and leend (that is how the data is segmented), while labels provide labels at the breakpoints.
If labels is set, breaks must be too.

```{r}
p <- qplot(cyl, wt, data=mtcars)
p 
p+scale_x_continuous(breaks=c(5.5,6.5))
p + scale_x_continuous(limits=c(5.5,6.5))
p <- qplot(wt,cyl,data=mtcars,colour=cyl)
p
p + scale_colour_gradient(breaks=c(5.5,6.5))
p + scale_colour_gradient(limits=c(5.5,6.5))

```

## Position Scales

